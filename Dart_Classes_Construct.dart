enum PersonType { student, employee }

class Person {
  late PersonType type;
  // Don't need "late" when use shortcut syntax
  String firstName;
  String lastName;
  static String personLabel = "Person name:";

  /*Person(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  */
  // Shortcut Syntax
  Person(this.firstName, this.lastName);

  //Factory Constructor
  /*factory Person.fromCache(String firstName, String lastName) {
    if (_cacheService.containsPerson(firstName, lastName)) {
      return _cacheService.getPerson(firstName, lastName);
    } else {
      return Person(firstName, lastName);
    }
  }*/

  String getFullName() => "$firstName $lastName";

  String get fullName => "$personLabel $firstName $lastName";
  String get initials => "${firstName[0]}.${lastName[0]}.";

  set fullName(String fullName) {
    var parts = fullName.split(" ");
    this.firstName = parts.first;
    this.lastName = parts.last;
  }

  static void printsPerson(Person person) {
    print("$personLabel ${person.firstName} ${person.lastName}");
  }
}

class Student extends Person {
  String nickName;
  Student(
    String firstName,
    String lastName,
    this.nickName,
  ) : super(firstName, lastName);
  @override
  String toString() => "$fullName, aka $nickName";
}

//Implement Person as interface sample

// class Student implements Person {
//   String nickName;
//   @override
//   String firstName;
//   @override
//   String lastName;
//   Student(this.firstName, this.lastName, this.nickName);
//   @override
//   String get fullName => "$firstName $lastName";
//   @override
//   String toString() => "$fullName, also known as $nickName";
// }

// Mixins
class ProgrammingSkills {
  coding() {
    print("writing code...");
  }
}

class ManagementSkills {
  manage() {
    print("managing project...");
  }
}

class SeniorDeveloper extends Person with ProgrammingSkills, ManagementSkills {
  SeniorDeveloper(String firstName, String lastName)
      : super(firstName, lastName);
}

class JuniorDeveloper extends Person with ProgrammingSkills {
  JuniorDeveloper(String firstName, String lastName)
      : super(firstName, lastName);
}

main() {
  //Person somePerson = Person();
  Person somePerson = Person("clark", "kent");
  somePerson.firstName = "Clark";
  somePerson.lastName = "Kent";
  print(somePerson.getFullName()); // prints Clark Kent

  print(somePerson.fullName); // prints Clark Kent
  print(somePerson.initials); // prints C. K.
  //somePerson.fullName = "peter parker";
  // we have not defined a setter fullName so it doesn't compile

  Person anotherPerson = Person("peter", "parker");
  //Person anotherPerson = Person();
  anotherPerson.firstName = "Peter";
  anotherPerson.lastName = "Parker";
  print(somePerson.fullName); // prints Person name: Clark kent
  print(anotherPerson.fullName); // prints Person name: Peter Parker
  Person.personLabel = "name:";
  print(somePerson.fullName); // prints name: Clark Kent
  print(anotherPerson.fullName); // prints name: PeterParker

  Person.printsPerson(anotherPerson);

  Student student = Student("Clark", "Kent", "Kal-El");
  print(student);

  print(PersonType.values);
  Person randomPerson = Person("ryan", "reynold");
  randomPerson.type = PersonType.employee;
  print(randomPerson.type);
  print(randomPerson.type.index);
  //print(describeEnum(PersonType.employee));
  
//   List placeNames = ["Middlesbrough", "New York"];
//   placeNames.add(1);
//   print("Place names: $placeNames");
  
  // Use of Generic (<String>)
  List<String> placeNames = ["Middlesbrough", "New York"];
  //placeNames.add(1); -> cause error
  print("Place names: $placeNames");
  
  // Generic and Dart Literals, Nullability in Generics
  var places = <String>["Middlesbrough", "New York"];
  var landmarks = <String, String?>{
    "Middlesbrough": "Transporter bridge",
    "New York": "Statue of Liberty",
    "Barnmouth": null,
  };
  
  var emptyStringArray = <String>[];
}
