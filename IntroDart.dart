void main() {
  for (int i = 0; i < 5; i++) {
    print("Hello ${i + 1}");
  }

  var inferredString = "Hello ";
  String explicitString = "World";

  print(inferredString + explicitString);

  int? newNumber; // Add ? to declare a nullable variable
  print(newNumber);
  newNumber = 4;
  print(newNumber);

  late int anotherNumber; // Different from ? -> no value
  //print(anotherNumber); // Can't do this
  anotherNumber = 9;
  print(anotherNumber);

  // NULL SAFETY
  int? goals;
  if (goals != null) {
    print(goals + 2);
  }

  // Null-aware methods
  String? goalScorer;
  print(goalScorer?.length); // Only called when goalScorer not null

  // Null-accertion operator
  int? goalTime;
  bool goalScored = false;
  if (goalScored) {
    goalTime = 21;
    goalScorer = "Bob";
  }
  if (goalTime != null) {
    print(goalScorer!.length);
  }

  //List examples
  List dynamicList = [];
  print(dynamicList.length); // Prints 0

  dynamicList.add("Hello");
  print(dynamicList[0]); // Prints "World"
  print(dynamicList.length); // Prints 1

  List preFilledDynamicList = [1, 2, 3];
  print(preFilledDynamicList[0]); // Prints 1
  print(preFilledDynamicList.length); // Prints 3

  List fixedList = List.filled(3, "World");
  // fixedList.add("Hello"); // Error
  fixedList[0] = "Hello";
  print(fixedList[0]); // Prints "Hello";
  print(fixedList[1]); // Prints "World";

  // Map example
  Map nameAgeMap = {};
  nameAgeMap["Alice"] = 23; // {"Alice": 23}
  print(nameAgeMap["Alice"]); // Prints 23

  // String examples
  String singleQuoteString = 'Here is a single quote string';
  String doubleQuoteString = "Here is a double quote string";

  String multiLineString = '''Here is a multi-line single
  quote string''';

  String str1 = 'Here is a ';
  String str2 = str1 + 'concatenated string';
  print(str2); // Prints Here is a concatenated string

  // String interpolation
  String someString = "Happy string";
  print("The string is: $someString");
  // prints The string is: Happy string
  // No curly brackets were required
  print("The string length is: ${someString.length}");
  // prints The string length is: 16
  // Curly brackets were required
  print("The string length is: $someString.length");
  // prints The string length is: Happy string.length
  // Omitting the curly brackets meant only the variable was evaluated, not the method on the variable.

  // Different between final and const:
  // const variable MUST be determined at compile time
  // Ex: const cannot be used for DateTime.now()
  //  since current Datetime can only be determined in runtime
  final int immutableInteger = 5;
  final double immutableDouble = 0.015;

  // Type annotation is optional
  final interpolatedInteger = 10;
  final interpolatedDouble = 72.8;
  print(interpolatedInteger);
  print(interpolatedDouble);

  const aFullySealedVariable = true;
  print(aFullySealedVariable);

  // If / else
  String test = "test2";
  if (test == "test1") {
    print("Test1");
  } else if (test == "test2") {
    print("Test2");
  } else {
    print("Something else");
  }
  // Prints Test2
  if (test == "test2") print("Test2 again"); // Prints Test 2

  // While, do-while
  int counter = 0;
  while (counter < 2) {
    print(counter);
    counter++;
  }
  // Prints 0, 1
  do {
    print(counter);
    counter++;
  } while (counter < 2);
  // Prints 2

  // For loop
  for (int index = 0; index < 2; index++) {
    print(index);
  }
  // Prints 0, 1

  // Break & Continue
  int counter2 = 0;
  while (counter2 < 10) {
    counter2++;
    if (counter2 == 4) {
      break;
    } else if (counter2 == 2) {
      continue;
    }
    print(counter2);
  }
  // Prints 1, 3
  
  // Function examples
  var helloFunction = sayHello();
  String helloMessage = helloFunction;
  print(helloMessage);
  
  optionalParameters();
}

String sayHello() {
  return "Hello world!";
}

void unnamed([String? name, int? age]){
  // ? after a parameter -> Nullable
  final actualName = name ?? 'Unknown'; // Shorthand if/ else 
  final actualAge = age ?? 0;
  print('$actualName is $actualAge ${actualAge == 1 ? 'year' : 'years'} old');
}

void named({String? greeting, String? name}) {
  // Need to specify parameter name, in any order.
  final actualGreeting = greeting ?? 'Hello';
  final actualName = name ?? 'Mystery Person';
  print('$actualGreeting, $actualName!');
}

String duplicate(String name, {int times = 1}) {
  // Default value instead of Null
  String merged = '';
  for (int i = 0; i < times; i++) {
    merged += name;
    if (i != times - 1) {
      merged += ' ';
    }
  }
  return merged;
}

void optionalParameters() {
  unnamed('Huxley',20);
  unnamed();
  // Notice how named parameters can be in any order
  named(greeting: 'Greetings and Salutations');
  named(name: 'Sonia');
  named(name: 'Alex',greeting: 'Bonjour');
  final multiply = duplicate('Mikey', times: 3);
  print(multiply);
}


