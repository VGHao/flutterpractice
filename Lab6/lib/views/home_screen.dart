import 'package:flutter/material.dart';
import 'package:food_list/utils/httphelper.dart';
import 'dart:convert';
import '../models/pizza.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: FutureBuilder(
        future: callPizzas(), //readJsonFile(context),
        builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
          return ListView.builder(
              itemCount: pizzas.data?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                var itemData = pizzas.data![index];
                return ListTile(
                  title: Text(itemData.pizzaName),
                  subtitle:
                      Text("${itemData.description} \$ ${itemData.price}"),
                );
              });
        },
      ),
    );
  }

  Future<List<Pizza>> readJsonFile(context) async {
    String myString = await DefaultAssetBundle.of(context)
        .loadString('assets/pizza_list.json');

    List myMap = jsonDecode(myString);
    List<Pizza> myPizzas = [];
    myMap.forEach((dynamic pizza) {
      Pizza myPizza = Pizza.fromJson(pizza);
      // Pizza myPizza = Pizza.fromJsonOrNull(pizza);
      myPizzas.add(myPizza);
    });

    return myPizzas;
  }

  Future<List<Pizza>> callPizzas() async {
    print('Use HttpHelper to get list of pizzas...');
    HttpHelper helper = HttpHelper();
    List<Pizza> pizzas = await helper.getPizzaList();
    return pizzas;
  }
}
