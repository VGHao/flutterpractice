library string_util;

import 'package:intl/intl.dart';

/// A Calculator.

String countText(String text) {
  int result = 0;
  for (int i = 0; i < text.length - 1; i++) {
    if (text[i] == ' ' && text[i + 1] != ' ') {
      result++;
    }
  }
  if (text[0] != ' ') {
    result++;
  }
  final formatter = NumberFormat('#');
  return formatter.format(result);
}
