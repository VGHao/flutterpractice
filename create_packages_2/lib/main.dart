import 'package:flutter/material.dart';
import 'package:bignumber_util/bignumber_util.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Packages Demo',
      home: PackageScreen(),
    );
  }
}

class PackageScreen extends StatefulWidget {
  const PackageScreen({Key? key}) : super(key: key);

  @override
  _PackageScreenState createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  final TextEditingController firstString = TextEditingController();
  final TextEditingController secondString = TextEditingController();
  String result = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Package App'),
      ),
      body: Column(
        children: [
          AppTextField(firstString, 'Nhập số thứ nhất'),
          AppTextField(secondString, 'Nhập số thứ hai'),
          const SizedBox(height: 18),
          ElevatedButton(
            child: const Text('Result'),
            onPressed: () {
              String firstNum = firstString.text;
              String secondNum = secondString.text;
              String res = sum(firstNum, secondNum);
              setState(() {
                result = res;
              });
            },
          ),
          const SizedBox(height: 18),
          Text(result),
        ],
      ),
    );
  }
}

class AppTextField extends StatelessWidget {
  final TextEditingController controller;
  final String label;

  const AppTextField(this.controller, this.label, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(hintText: label),
      ),
    );
  }
}
