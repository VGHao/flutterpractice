import 'package:flutter/material.dart';
import '../stream/word_stream.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  WordStream wordStream = WordStream();
  String englishWord = 'Hello';
  int _counter = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Words',

      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        body: Center(
          child: Column(
            children: [
              Text("Random Words"),
              Text(englishWord),
              Text("Counter: $_counter"),
            ],
          ),

        ),
        floatingActionButton: FloatingActionButton(
          child: Text('Start'),
          onPressed: () {
            changeWord();
            count();
          },
        ),
      ),
    );
  }

  changeWord() async {
    wordStream.getWord().listen((eventWord) {
      setState(() {
        print(eventWord);
        englishWord = eventWord!;
      });
    });
  }

  count() async {
    wordStream.countWord().listen((eventCount) {
      setState(() {
        print("Counter : $eventCount");
        _counter = eventCount!;
      });
    });
  }
}