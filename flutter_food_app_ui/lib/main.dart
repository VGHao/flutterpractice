import 'package:flutter/material.dart';
import 'package:flutter_food_app_ui/screens/home/home_screen.dart';
import 'package:flutter_food_app_ui/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // SizeConfig().init(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food App UI',
      theme: buildThemeData(),

      /// On first screen must call [SizeConfig().init(context)]
      home: const HomeScreen(),
    );
  }
}
