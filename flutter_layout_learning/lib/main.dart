import 'package:flutter/material.dart';
import 'package:flutter_layout_learning/deep_tree.dart';
import 'package:flutter_layout_learning/e_commerce_screen_before.dart';
import 'package:flutter_layout_learning/flex_screen.dart';
import 'package:flutter_layout_learning/profile_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.green,
        appBarTheme: AppBarTheme(
          elevation: 10,
          textTheme: TextTheme(
            headline6: TextStyle(
              fontFamily: 'LeckerliOne',
              fontSize: 24,
            ),
          ),
        ),
      ),
      // home: FlexScreen(),
      // home: ProfileScreen(),
      // home: DeepTree(),
      home: ECommerceScreen(),
    );
  }
}
