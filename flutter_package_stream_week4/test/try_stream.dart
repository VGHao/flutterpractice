import 'dart:async';

class Bloc{
  final streamController = StreamController<String>();
  //Capture data
  // void receiveData(String msg) {
  //   streamController.sink.add(msg);
  // }

  // Capture data: use getter
  get receiveData => streamController.sink.add;

  // :isten and process received data
  Bloc(){
    streamController.stream.listen((event) {
      print(event);
    });
  }
}

void main(){
  Bloc a = Bloc();

  a.receiveData('Hello');
  //a.streamController.sink.add('Hello');

  a.receiveData('Stream');
  a.receiveData('Week 4');
}