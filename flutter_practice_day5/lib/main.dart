import 'package:flutter/material.dart';
import 'package:flutter_practice_day5/plan_provider.dart';
import 'package:flutter_practice_day5/views/plan_creator_screen.dart';

void main() => runApp(PlanProvider(child: const MasterPlanApp()));

class MasterPlanApp extends StatelessWidget {
  const MasterPlanApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.purple),
      home: const PlanCreatorScreen(),
    );
  }
}
