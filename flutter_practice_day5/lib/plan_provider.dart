import 'package:flutter/material.dart';
import './models/data_layer.dart';
import 'controllers/plan_controller.dart';

class PlanProvider extends InheritedWidget {
  final _controller = PlanController();
  PlanProvider({Key? key, required this.child}) : super(key: key, child: child);

  @override
  final Widget child;
  final _plans = <Plan>[];

  static PlanController of(BuildContext context) {
    PlanProvider? provider =
        context.dependOnInheritedWidgetOfExactType<PlanProvider>();
    return provider!._controller;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
