import 'package:flutter/material.dart';
import 'package:flutter_stopwatch_week6/ui/app.dart';

void main() => runApp(const StopwatchApp());
