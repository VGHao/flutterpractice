import 'package:flutter/material.dart';
import 'package:flutter_stopwatch_week6/ui/stopwatch_screen.dart';

import 'login_screen.dart';

class StopwatchApp extends StatelessWidget {
  const StopwatchApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => LoginScreen(),
        '/login': (context) => LoginScreen(),
        StopwatchScreen.route: (context) => StopwatchScreen()
      },
      initialRoute: '/',
    );
  }
}
