import 'package:flutter/material.dart';

import 'main_screen.dart';

class ZaloClone extends StatelessWidget {
  const ZaloClone({Key? key}) : super(key: key);

  // This widget is the root of my application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of my application.
        primarySwatch: Colors.blue,
      ),
      home: const MainScreen(),
    );
  }
}
