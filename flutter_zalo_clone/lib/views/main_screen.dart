import 'package:flutter/material.dart';
import 'page_contacts/screen_contacts.dart';
import 'page_discovery/screen_discovery.dart';
import 'page_me/screen_me.dart';
import 'page_messages/screen_messages.dart';
import 'page_timeline/screen_timeline.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _pageIndex = 0;
  late PageController _pageController;
  List<String> items = [];

  List<Widget> tabPages = [
    MessagesPage(),
    ContactsPage(),
    DiscoveryPage(),
    TimelinePage(),
    MePage(),
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _pageIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appbar(),
      backgroundColor: Colors.grey.shade100,
      body: PageView(
        children: tabPages,
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  Widget _bottomNavigationBar() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 0.5, color: Colors.grey.shade300),
        ),
      ),
      child: BottomNavigationBar(
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.black,
        items: <BottomNavigationBarItem>[
          _buildBottomNavItem(Icons.message_outlined, 'Messages'),
          _buildBottomNavItem(Icons.contact_page_outlined, 'Contacts'),
          _buildBottomNavItem(Icons.widgets_outlined, 'Discovery'),
          _buildBottomNavItem(Icons.access_time_outlined, 'Timeline'),
          _buildBottomNavItem(Icons.person_outline, 'Me'),
        ],
        currentIndex: _pageIndex,
        onTap: onTabTapped,
      ),
    );
  }

  BottomNavigationBarItem _buildBottomNavItem(IconData icon, String text) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: text,
    );
  }

  void onPageChanged(int page) {
    setState(() {
      _pageIndex = page;
    });
  }

  void onTabTapped(int index) {
    _pageController.animateToPage(index,
        duration: const Duration(milliseconds: 100), curve: Curves.easeInOut);
  }

  AppBar _appbar() {
    return AppBar(
      toolbarHeight: 50,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              Color(0xFF257CFF),
              Color(0xFF02BAFA),
            ],
          ),
        ),
      ),
      leading: IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
      title: const Text(
        'Search friends, messages... ',
        style: TextStyle(
            color: Colors.white54, fontSize: 14, fontWeight: FontWeight.w400),
      ),
      actions: [
        IconButton(
            onPressed: () {}, icon: const Icon(Icons.qr_code_scanner_rounded)),
        PopupMenuButton<String>(
          icon: const Icon(Icons.add),
          itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
            _buildMenuItem(Icons.group_add_outlined, 'Create group'),
            _buildMenuItem(Icons.person_add_alt_1_outlined, 'Add friend'),
            _buildMenuItem(Icons.desktop_windows_outlined, 'Login history'),
            _buildMenuItem(Icons.calendar_month_outlined, 'Zalo Calendar'),
            _buildMenuItem(Icons.cloud_outlined, 'My Cloud'),
            _buildMenuItem(Icons.videocam_outlined, 'Create group call'),
          ],
        ),
      ],
    );
  }

  PopupMenuItem<String> _buildMenuItem(IconData icon, String text) {
    return PopupMenuItem<String>(
      child: Row(
        children: [
          Icon(icon, color: Colors.black),
          const SizedBox(width: 8),
          Text(text),
        ],
      ),
    );
  }
}
