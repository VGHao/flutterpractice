import 'package:flutter/material.dart';

class FriendTab extends StatelessWidget {
  const FriendTab({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          tileColor: Colors.white,
          leading: Icon(Icons.people_rounded),
          title: Text('Friend request'),
          onTap: () {},
        ),
        ListTile(
          tileColor: Colors.white,
          leading: Icon(Icons.contact_phone),
          title: Text('Phonebook'),
          subtitle: Text('Contacts who use Zalo'),
          onTap: () {},
        ),
        SizedBox(height: 8),
        Container(
          decoration: BoxDecoration(color: Colors.white),
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                children: [
                  Icon(Icons.star, color: Colors.yellow),
                  Text(
                    'Close friends',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Row(
                      children: [
                        Icon(Icons.add, color: Colors.blue),
                        Text("Add", style: TextStyle(color: Colors.blue)),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 18,
                    backgroundImage: NetworkImage(
                        'https://s3.o7planning.com/images/boy-128.png'),
                  ),
                  SizedBox(width: 8),
                  Expanded(child: Text('Random Guys')),
                  IconButton(onPressed: () {}, icon: Icon(Icons.call_outlined)),
                  IconButton(
                      onPressed: () {}, icon: Icon(Icons.videocam_outlined)),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
