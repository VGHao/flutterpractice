import 'package:flutter/material.dart';

import 'components/FriendTab.dart';
import 'components/GroupTab.dart';
import 'components/OASTab.dart';

class ContactsPage extends StatelessWidget {
  const ContactsPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DefaultTabController(
      initialIndex: 1,
      length: 3,
      child: Scaffold(
        appBar: TabBar(
          labelColor: Colors.black,
          labelStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 12),
          tabs: <Widget>[
            Tab(
              text: 'FRIENDS',
            ),
            Tab(
              text: 'GROUPS',
            ),
            Tab(
              text: 'OAS',
            ),
          ],
        ),
        body: TabBarView(
          children: <Widget>[
            FriendTab(),
            GroupTab(),
            OASTab(),
          ],
        ),
      ),
    );
  }
}
