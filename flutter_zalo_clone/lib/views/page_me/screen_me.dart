import 'package:flutter/material.dart';

class MePage extends StatelessWidget {
  const MePage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildUserTile("https://s3.o7planning.com/images/boy-128.png", "User"),
        const SizedBox(height: 8),
        _buildSubTile(Icons.account_balance_wallet_outlined, 'QR Wallet',
            'Save and user important QR codes'),
        const SizedBox(height: 8),
        _buildSubTile(
            Icons.cloud_outlined, 'My Cloud', 'Keep important messages'),
        const SizedBox(height: 8),
        _buildTile(Icons.shield_outlined, 'Account and security'),
        const Divider(height: 0.2),
        _buildTile(Icons.lock_outlined, 'Privacy'),
      ],
    );
  }

  ListTile _buildUserTile(String imageUrl, String userName) {
    return ListTile(
      tileColor: Colors.white,
      onTap: () {},
      leading: CircleAvatar(
        radius: 22,
        backgroundImage: NetworkImage(imageUrl),
      ),
      title: Text(userName),
      subtitle: const Text('View my profile'),
      trailing: IconButton(
        icon: const Icon(Icons.manage_accounts_outlined, color: Colors.blue),
        onPressed: () {},
      ),
    );
  }

  ListTile _buildSubTile(IconData icon, String _title, String _subtitle) {
    return ListTile(
      tileColor: Colors.white,
      onTap: () {},
      leading: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: Colors.blue,
          ),
        ],
      ),
      title: Text(_title),
      subtitle: Text(_subtitle),
    );
  }

  ListTile _buildTile(IconData icon, String _title) {
    return ListTile(
      tileColor: Colors.white,
      onTap: () {},
      leading: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: Colors.blue,
          ),
        ],
      ),
      title: Text(_title),
    );
  }
}
