import 'dart:developer';

import 'package:flutter/material.dart';

class MessagesList extends StatefulWidget {
  const MessagesList({
    Key? key,
    required this.avaImageUrl,
    required this.friendsName,
    required this.friendsMess,
    required this.formatted,
  }) : super(key: key);

  final String avaImageUrl;
  final String friendsName;
  final String friendsMess;
  final String formatted;

  @override
  State<MessagesList> createState() => _MessagesListState();
}

class _MessagesListState extends State<MessagesList> {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: 10,
      itemBuilder: (context, index) {
        return ListTile(
          leading: CircleAvatar(
            radius: 22,
            backgroundImage: NetworkImage(widget.avaImageUrl),
          ),
          title: Text(
            widget.friendsName,
            style: const TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
            ),
          ),
          subtitle: Text(
            widget.friendsMess,
            style: const TextStyle(fontSize: 14),
          ),
          trailing: Text(widget.formatted,
              style: const TextStyle(color: Colors.grey, fontSize: 12)),
          onTap: () => log('$index'),
          onLongPress: () => {
            log('Long press: $index'),
            // showMenu(
            //     context: context,
            //     position: RelativeRect.fromLTRB(25.0, 25.0, 0.0, 0.0),
            //     elevation: 8.0,
            //     items: [PopupMenuItem(value: 'delete', child: Text('Delete'))])
          },
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return const Divider(
          height: 0,
        );
      },
    );
  }
}
