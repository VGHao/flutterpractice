import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'components/messages_list.dart';

class MessagesPage extends StatelessWidget {
  const MessagesPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd/MM');
    final String formatted = formatter.format(now);

    String avaImageUrl = "https://s3.o7planning.com/images/boy-128.png";
    String friendsName = 'Random Guys';
    String friendsMess = 'Random Messages';

    return MessagesList(
        avaImageUrl: avaImageUrl,
        friendsName: friendsName,
        friendsMess: friendsMess,
        formatted: formatted);
  }
}
