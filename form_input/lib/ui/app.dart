import 'package:flutter/material.dart';
import '../validation/mixins_validation.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Input Form',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Infomation Form'),
        ),
        body: FormInputScreen(),
      ),
    );
  }
}

class FormInputScreen extends StatefulWidget {
  const FormInputScreen({Key? key}) : super(key: key);

  @override
  _FormInputScreenState createState() => _FormInputScreenState();
}

class _FormInputScreenState extends State<FormInputScreen> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String last_midName;
  late String firstName;
  late int yearOfBirth;
  late String homeAddress;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            fieldEmailAddress(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            fieldLastMidName(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            fieldFirstName(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),fieldYearOfBirth(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),fieldHomeAddress(),
            
            saveBtn(),
          ],
        ));
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: "Email address",
      ),
      validator: validateEmail,

      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldLastMidName() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        icon: Icon(Icons.abc),
        labelText: "Last(Mid) Name",
      ),
      validator: validateText,

      onSaved: (value) {
        last_midName = value as String;
      },
    );
  }

  Widget fieldFirstName() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        icon: Icon(Icons.abc),
        labelText: "First Name",
      ),
      validator: validateText,

      onSaved: (value) {
        firstName = value as String;
      },
    );
  }

  Widget fieldYearOfBirth() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: Icon(Icons.calendar_month),
        labelText: "Year of Birth",
      ),
      validator: validateYear,

      onSaved: (value) {
        yearOfBirth = int.parse(value!);
      },
    );
  }

  Widget fieldHomeAddress() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        icon: Icon(Icons.home),
        labelText: "Home Address",
      ),
      validator: validateText,

      onSaved: (value) {
        homeAddress = value as String;
      },
    );
  }

  Widget saveBtn() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            //Call API Auth from Backend
            formKey.currentState!.save();
            print('$emailAddress\n$last_midName\n$firstName\n$yearOfBirth\n$homeAddress');
          }
          print('Clicked save button');
        },
        child: Text('Save'));
  }


}
