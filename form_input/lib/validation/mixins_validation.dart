mixin CommonValidation{
  String? validateEmail(String? value){
    if (value == null || value.isEmpty){
      return 'Please enter an email';
    }
    if (!value.contains('@')) {
      return 'Please input valid email.';
    }
    return null;
  }

  String? validateText(String? value){
    if (value == null || value.isEmpty){
      return 'Please enter a name';
    }
    return null;
  }

  String? validateYear(String? value){
    if (value == null || value.isEmpty){
      return 'Please enter a year';
    }
    final n = int.tryParse(value);
    if(n == null) {
      return '"$value" is not a valid year';
    }
    if (int.parse(value) < 1800 || int.parse(value) > 2022){
      return 'Please input valid year';
    }
    return null;
  }

  String? validatePassword(String? value){
    if (value!.length < 6) {
      return 'Password must be at least 6 chars';
    }
    return null;
  }
}