import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../model/image_model.dart';
import 'dart:convert';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  List<ImageModel> imgList = [];
  int counter = 1;

  fetchImage() async {
    print('Hi there! counter=$counter');

    Uri url = Uri.https('jsonplaceholder.typicode.com', 'photos/$counter');
    // 'https://jsonplaceholder.typicode.com/photos/1';

    // http.get(url).then((result) {
    //   var jsonRaw = result.body;
    //   var jsonObject = json.decode(jsonRaw);
    //
    //   var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
    //   //var imageModel = ImageModel.fromJson(jsonObject);
    //   imgList.add(imageModel);
    //
    //   print(imageModel);
    //   print(imgList);
    // });

    var result = await http.get(url);
    var jsonRaw = result.body;
    var jsonObject = json.decode(jsonRaw);
    var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
    imgList.add(imageModel);
    print(imageModel);
    print(imgList);

    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('Image Viewer - v0.0.8'),
      ),
      body: //Text('Display list of images here. counter = ${counter - 1}'),
          ListView.builder(
        itemCount: imgList.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Image.network(imgList[index].getUrl),
          );
        },
      ),
      floatingActionButton:
          FloatingActionButton(child: Icon(Icons.add), onPressed: fetchImage),
    ));

    return appWidget;
  }
}
