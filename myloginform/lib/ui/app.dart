import 'package:flutter/material.dart';
import '../validation/mixins_validation.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Login Bar'),
        ),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              fieldEmailAddress(),
              Container(
                margin: EdgeInsets.only(top: 20),
              ),
              fieldPassword(),
              Container(
                margin: EdgeInsets.only(top: 20),
              ),
              loginBtn(),
            ],
          )),
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: "Email address",
      ),
      validator: validateEmail,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.password),
        labelText: "Password",
      ),
      validator: validatePassword,
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginBtn() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            //Call API Auth from Backend
            formKey.currentState!.save();
            print('$emailAddress, Demo only: $password');
          }
          print('Clicked login button');
        },
        child: Text('Login'));
  }
}
