mixin CommonValidation{
  String? validateEmail(String? value){
    if (!value!.contains('@')) {
      return 'Please input valid email.';
    }
    return null;
  }

  String? validatePassword(String? value){
    if (value!.length < 6) {
      return 'Password must be at least 6 chars';
    }
    return null;
  }
}