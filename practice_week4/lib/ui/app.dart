import 'package:flutter/material.dart';
import 'package:practice_week4/ui/zalo_home.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Zalo",
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      home: const ZaloHome(),
    );
  }
}
