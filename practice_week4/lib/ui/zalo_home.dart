import 'package:flutter/material.dart';

class ZaloHome extends StatefulWidget {
  const ZaloHome({Key? key}) : super(key: key);

  @override
  State<ZaloHome> createState() => _ZaloHomeState();
}

class _ZaloHomeState extends State<ZaloHome> {
  final List<String> userList = <String>['User 1', 'User 2', 'User 3'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Zalo"),
        // elevation: 0.7,
      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(8),
        itemCount: userList.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
              onTap: () {
                //print("Item $index clicked");
              },
              child: ListTile(
                leading: const CircleAvatar(
                  backgroundImage: NetworkImage(
                      'http://assets.stickpng.com/images/585e4bf3cb11b227491c339a.png'),
                ),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      userList[index],
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Time",
                      style: TextStyle(color: Colors.grey, fontSize: 14.0),
                    ),
                  ],
                ),
                subtitle: Container(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: const Text(
                    "Old Messages",
                    style: TextStyle(color: Colors.grey, fontSize: 14.0),
                  ),
                ),
              ));
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.message),
            label: 'Messages',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.contacts),
            label: 'Contacts',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Me',
          ),
        ],
        // currentIndex: _selectedIndex,
        // selectedItemColor: Colors.amber[800],
        // onTap: _onItemTapped,
      ),
    );
  }
}
