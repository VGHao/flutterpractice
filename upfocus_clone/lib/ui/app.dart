import 'package:avatars/avatars.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:upfocus_clone/ui/profile/profile.dart';
import 'package:upfocus_clone/ui/timer/timer.dart';

import 'goals/goals.dart';
import 'home/home.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'UpFocus Clone',
      theme: ThemeData(
        // This is the theme of your application.
        primarySwatch: Colors.blue,
        textTheme: GoogleFonts.robotoTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
      home: const MyTabView(),
    );
  }
}

class MyTabView extends StatefulWidget {
  const MyTabView({Key? key}) : super(key: key);

  @override
  State<MyTabView> createState() => _MyTabViewState();
}

class _MyTabViewState extends State<MyTabView> {
  final List<String> titleList = ["Your Focus", "Timer", "Goals", "Profile"];
  int _selectedIndex = 0;
  late String currentTitle;

  @override
  void initState() {
    currentTitle = titleList[_selectedIndex];
    super.initState();
  }

  // 3 main tabs
  static final List<Widget> _widgetOptions = <Widget>[
    const HomeTab(),
    const TimerTab(),
    const GoalsTab(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      currentTitle = titleList[_selectedIndex];
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: const Center(
            child: Text(
              'UP',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 38,
                  color: Colors.black),
            ),
          ),
          title: Text(
            currentTitle,
            style: const TextStyle(fontSize: 20, color: Colors.black),
          ),
          actions: [
            SizedBox(
              height: 80,
              width: 80,
              child: IconButton(
                icon: Avatar(
                  name: 'Hao',
                  shape: AvatarShape.rectangle(
                      40, 40, const BorderRadius.all(Radius.circular(8.0))),
                ),
                iconSize: 14,
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ProfilePage()),
                  );
                },
              ),
            ),
          ],
          centerTitle: true,
        ),
        body: Container(
          decoration: const BoxDecoration(color: Color(0xFFF5F6FB)),
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: 'home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.timer_outlined),
              label: 'timer',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'goal',
            ),
          ],
          showSelectedLabels: false,
          showUnselectedLabels: false,
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.blue,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
