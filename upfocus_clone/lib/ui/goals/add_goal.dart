import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AddGoal extends StatefulWidget {
  const AddGoal({Key? key}) : super(key: key);

  @override
  State<AddGoal> createState() => _AddGoalState();
}

class _AddGoalState extends State<AddGoal> {
  final textController = TextEditingController();
  bool status = false;
  bool focusChecked = false;

  DateTime selectedDate = DateTime.now();
  String formattedDate = DateFormat("dd.MM.yyyy").format(DateTime.now());

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2030),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat("dd.MM.yyyy").format(selectedDate);
      });
    }
  }

  _onChanged(String value) {
    if (value.isNotEmpty) {
      setState(() {
        status = true;
      });
    } else {
      setState(() {
        status = false;
      });
    }
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color(0xFFF5F6FB),
        body: Column(
          children: [_buildAppBar(context), _buildGoalInputField()],
        ),
      ),
    );
  }

  Widget _buildGoalInputField() {
    return Container(
      padding: const EdgeInsets.all(32.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 24,
                width: 24,
                child: Checkbox(
                  checkColor: Colors.white,
                  fillColor:
                      MaterialStateColor.resolveWith((states) => Colors.blue),
                  value: focusChecked,
                  shape: const CircleBorder(),
                  onChanged: (bool? value) {
                    setState(() {
                      focusChecked = value!;
                    });
                  },
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: Text(
                  'Focus on this goal',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
                ),
              )
            ],
          ),
          const SizedBox(height: 16.0),
          const Text(
            'Name of the goal',
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: const BorderRadius.all(Radius.circular(12.0)),
            ),
            child: TextField(
              cursorWidth: 1.0,
              controller: textController,
              decoration: const InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(16.0),
                hintText: 'Goal',
              ),
              onChanged: _onChanged,
            ),
          ),
          const SizedBox(height: 16.0),
          const Text(
            'Deadline',
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          InkWell(
            onTap: () => _selectDate(context),
            child: Container(
              padding: const EdgeInsets.all(16.0),
              margin: const EdgeInsets.symmetric(vertical: 16.0),
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: const BorderRadius.all(Radius.circular(12.0)),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Icon(
                    Icons.calendar_today_outlined,
                    size: 20.0,
                  ),
                  const SizedBox(width: 12.0),
                  Text(formattedDate),
                ],
              ),
            ),
          ),
          const SizedBox(height: 16.0),
          const Text(
            'Reward for performance',
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: const BorderRadius.all(Radius.circular(12.0)),
            ),
            child: const TextField(
              cursorWidth: 1.0,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(16.0),
                hintText: 'Reward',
              ),
            ),
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              const Text(
                'Photo of the goal',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: const EdgeInsets.only(left: 8.0),
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                ),
                child: const Text(
                  'PRO',
                  style: TextStyle(color: Colors.white, fontSize: 12.0),
                ),
              ),
            ],
          ),
          Container(
            padding: const EdgeInsets.all(16.0),
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: const BorderRadius.all(Radius.circular(12.0)),
            ),
            child: const Icon(Icons.add_photo_alternate_outlined, size: 36.0),
          ),
        ],
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back_ios_new_rounded),
                iconSize: 32,
              ),
              const Spacer(),
              Visibility(
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true,
                visible: status,
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.check_rounded,
                    color: Colors.blue,
                  ),
                  iconSize: 32,
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              'Add a goal',
              style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
