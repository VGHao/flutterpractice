import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:upfocus_clone/ui/goals/add_goal.dart';

class GoalsTab extends StatefulWidget {
  const GoalsTab({Key? key}) : super(key: key);

  @override
  State<GoalsTab> createState() => _GoalsTabState();
}

class _GoalsTabState extends State<GoalsTab> {
  late final List<bool> isSelected = [true, false];
  int _initialLabelIndex = 0;
  int selectedTab = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildIntro(context),
        const SizedBox(height: 16.0),
        _buildAddGoalButton(),
        const SizedBox(height: 24.0),
        _buildToggle(context),
        const SizedBox(height: 32.0),
        selectedTab == 0 ? _buildActiveGoals() : _buildDoneGoals(),
      ],
    );
  }

  Widget _buildToggle(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ToggleSwitch(
            minWidth: MediaQuery.of(context).size.width / 2.3,
            minHeight: 50.0,
            fontSize: 16.0,
            initialLabelIndex: _initialLabelIndex,
            activeBgColor: const [Colors.white],
            activeFgColor: Colors.blue,
            inactiveBgColor: Colors.white,
            inactiveFgColor: Colors.black,
            totalSwitches: 2,
            labels: const ['Active', 'Done'],
            onToggle: (index) {
              log('switched to: $index');
              setState(() {
                selectedTab = index!;
                _initialLabelIndex = index;
              });
            },
          ),
        ],
      ),
    );
  }

  Widget _buildAddGoalButton() {
    return Container(
      margin: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: TextButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
          overlayColor: MaterialStateProperty.resolveWith<Color?>(
            (Set<MaterialState> states) {
              if (states.contains(MaterialState.hovered)) {
                return Colors.black.withOpacity(0.04);
              }
              if (states.contains(MaterialState.focused) ||
                  states.contains(MaterialState.pressed)) {
                return Colors.black.withOpacity(0.12);
              }
              return null; // Defer to the widget's default.
            },
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AddGoal()));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.add,
                size: 22.0,
              ),
              Text(
                'Add a goal',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w100),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildIntro(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
        color: Colors.white,
      ),
      child: const Text(
        'Create a goal and start achieving it with sprints!',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16.0,
        ),
      ),
    );
  }

  Widget _buildActiveGoals() => Container(
        margin: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(left: 10.0),
              child: Row(
                children: const [
                  Icon(
                    Icons.check_circle,
                    color: Colors.blue,
                    size: 18.0,
                  ),
                  SizedBox(width: 8.0),
                  Text(
                    'Current focus',
                    style: TextStyle(color: Colors.blue, fontSize: 14.0),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 12.0),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(16.0),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(12.0),
                ),
                color: Colors.white,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      const Text(
                        'Goal',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16.0),
                      ),
                      const Spacer(),
                      ConstrainedBox(
                        constraints: const BoxConstraints.tightFor(
                            width: 24, height: 24),
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ButtonStyle(
                            elevation: MaterialStateProperty.all(0),
                            shape:
                                MaterialStateProperty.all(const CircleBorder()),
                            padding: MaterialStateProperty.all(EdgeInsets.zero),
                            backgroundColor: MaterialStateProperty.all(
                                Colors.blue[100]), // <-- Button color
                            overlayColor:
                                MaterialStateProperty.resolveWith<Color?>(
                                    (states) {
                              if (states.contains(MaterialState.pressed)) {
                                return Colors.blue;
                              }
                              return null;
                            }),
                          ),
                          child: const Icon(Icons.check, size: 14.0),
                        ),
                      ),
                      const SizedBox(width: 12.0),
                      ConstrainedBox(
                        constraints: const BoxConstraints.tightFor(
                            width: 24, height: 24),
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ButtonStyle(
                            elevation: MaterialStateProperty.all(0),
                            shape:
                                MaterialStateProperty.all(const CircleBorder()),
                            padding: MaterialStateProperty.all(EdgeInsets.zero),

                            backgroundColor: MaterialStateProperty.all(
                                Colors.blue), // <-- Button color
                            overlayColor:
                                MaterialStateProperty.resolveWith<Color?>(
                                    (states) {
                              if (states.contains(MaterialState.pressed)) {
                                return Colors.blue[100];
                              }
                              return null;
                            }),
                          ),
                          child: const Icon(Icons.edit_outlined, size: 14.0),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8.0),
                  const Text('0/1 sprints', style: TextStyle(fontSize: 12.0)),
                  const SizedBox(height: 4.0),
                  ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    child: LinearProgressIndicator(
                      value: 0.0,
                      backgroundColor: Colors.blue[100],
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  Row(
                    children: const [
                      Spacer(),
                      Icon(
                        Icons.calendar_today_outlined,
                        color: Colors.blue,
                        size: 14.0,
                      ),
                      SizedBox(width: 4.0),
                      Text('17.05.2022', style: TextStyle(fontSize: 12.0)),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      );

  Widget _buildDoneGoals() => Container(
        margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 150.0),
        padding: const EdgeInsets.symmetric(vertical: 28.0),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(12.0),
          ),
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              'No completed goals',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22.0),
            ),
          ],
        ),
      );
}
