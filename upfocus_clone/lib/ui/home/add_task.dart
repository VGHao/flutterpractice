import 'package:flutter/material.dart';

class AddTask extends StatefulWidget {
  const AddTask({Key? key}) : super(key: key);

  @override
  State<AddTask> createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  final textController = TextEditingController();
  bool status = false;
  bool podoChecked = true;
  int podoCount = 5;

  _onChanged(String value) {
    if (value.isNotEmpty) {
      setState(() {
        status = true;
      });
    } else {
      setState(() {
        status = false;
      });
    }
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color(0xFFF5F6FB),
        body: Column(
          children: [_buildAppBar(context), _buildTaskInputField()],
        ),
      ),
    );
  }

  Widget _buildTaskInputField() {
    return Container(
      padding: const EdgeInsets.all(32.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Name of the task',
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: const BorderRadius.all(Radius.circular(12.0)),
            ),
            child: TextField(
              cursorWidth: 1.0,
              controller: textController,
              decoration: const InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(16.0),
                hintText: 'Task',
              ),
              onChanged: _onChanged,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 24,
                width: 24,
                child: Checkbox(
                  checkColor: Colors.white,
                  fillColor:
                      MaterialStateColor.resolveWith((states) => Colors.blue),
                  value: podoChecked,
                  shape: const CircleBorder(),
                  onChanged: (bool? value) {
                    setState(() {
                      podoChecked = value!;
                    });
                  },
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: Text(
                  'Podomoro',
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
          Visibility(
            visible: podoChecked,
            child: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Number of Podomoro cycles',
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 12),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        podoCount.toString(),
                        style: const TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(width: 10.0),
                      _buildMinusButton(),
                      const SizedBox(width: 10.0),
                      _buildAddButton(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back_ios_new_rounded),
                iconSize: 32,
              ),
              const Spacer(),
              Visibility(
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true,
                visible: status,
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.check_rounded,
                    color: Colors.blue,
                  ),
                  iconSize: 32,
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              'Add a task',
              style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildMinusButton() {
    return ConstrainedBox(
      constraints: const BoxConstraints.tightFor(width: 30, height: 30),
      child: ElevatedButton(
        onPressed: () {
          if (podoCount > 0) {
            setState(() {
              podoCount--;
            });
          }
        },
        style: ButtonStyle(
          elevation: MaterialStateProperty.all(0),
          shape: MaterialStateProperty.all(const CircleBorder()),
          padding: MaterialStateProperty.all(EdgeInsets.zero),

          backgroundColor:
              MaterialStateProperty.all(Colors.blue), // <-- Button color
          overlayColor: MaterialStateProperty.resolveWith<Color?>((states) {
            if (states.contains(MaterialState.pressed)) {
              return Colors.blue;
            }
            return null;
          }),
        ),
        child: const Icon(Icons.remove, size: 22.0),
      ),
    );
  }

  Widget _buildAddButton() {
    return ConstrainedBox(
      constraints: const BoxConstraints.tightFor(width: 30, height: 30),
      child: ElevatedButton(
        onPressed: () {
          setState(() {
            podoCount++;
          });
        },
        style: ButtonStyle(
          elevation: MaterialStateProperty.all(0),
          shape: MaterialStateProperty.all(const CircleBorder()),
          padding: MaterialStateProperty.all(EdgeInsets.zero),

          backgroundColor:
              MaterialStateProperty.all(Colors.blue), // <-- Button color
          overlayColor: MaterialStateProperty.resolveWith<Color?>((states) {
            if (states.contains(MaterialState.pressed)) {
              return Colors.blue;
            }
            return null;
          }),
        ),
        child: const Icon(Icons.add, size: 22.0),
      ),
    );
  }
}
