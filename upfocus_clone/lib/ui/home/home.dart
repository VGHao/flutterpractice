import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:upfocus_clone/ui/home/add_task.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key? key}) : super(key: key);

  @override
  State<HomeTab> createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  late final List<bool> isSelected = [true, false];
  int _initialLabelIndex = 0;
  int selectedTab = 0;
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildGoal(context),
        const SizedBox(height: 28.0),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              _buildSprint(),
              const SizedBox(height: 28.0),
              _buildToggle(),
              const SizedBox(height: 16.0),
              selectedTab == 0 ? _buildActiveTasks() : _buildDoneTasks(),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildGoal(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24.0),
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Goal',
            textAlign: TextAlign.left,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          const SizedBox(height: 6.0),
          const Text(
            '0/1 sprints',
            style: TextStyle(fontSize: 14.0),
          ),
          const SizedBox(height: 4.0),
          ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: LinearProgressIndicator(
              value: 0.0,
              backgroundColor: Colors.blue[100],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSprint() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(20.0),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(12.0),
        ),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: const [
              Text(
                'Sprint',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
              ),
              Spacer(),
              Text('0/1 tasks', style: TextStyle(fontSize: 12.0)),
            ],
          ),
          const SizedBox(height: 12.0),
          ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: LinearProgressIndicator(
              minHeight: 8.0,
              value: 0.0,
              backgroundColor: Colors.blue[100],
            ),
          ),
          const SizedBox(height: 12.0),
          Row(
            children: const [
              Icon(
                Icons.calendar_today_outlined,
                color: Colors.blue,
                size: 12.0,
              ),
              SizedBox(width: 8.0),
              Text('17.05.2022',
                  style:
                      TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold)),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildToggle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ToggleSwitch(
          minWidth: MediaQuery.of(context).size.width / 2.3,
          minHeight: 50.0,
          fontSize: 14.0,
          initialLabelIndex: _initialLabelIndex,
          activeBgColor: const [Colors.white],
          activeFgColor: Colors.blue,
          inactiveBgColor: Colors.white,
          inactiveFgColor: Colors.black,
          totalSwitches: 2,
          labels: const ['Active', 'Done'],
          onToggle: (index) {
            log('switched to: $index');
            setState(() {
              selectedTab = index!;
              _initialLabelIndex = index;
            });
          },
        ),
      ],
    );
  }

  Widget _buildActiveTasks() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(12.0),
        ),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Checkbox(
            checkColor: Colors.white,
            fillColor: MaterialStateProperty.all(Colors.blue),
            value: isChecked,
            shape: const CircleBorder(),
            onChanged: (bool? value) {
              setState(() {
                isChecked = value!;
              });
            },
          ),
          Flexible(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      'Task',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 14.0),
                    ),
                    Text(
                      '0/5',
                      style: TextStyle(color: Colors.blue, fontSize: 12.0),
                    ),
                  ],
                ),
                const SizedBox(height: 6.0),
                ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  child: LinearProgressIndicator(
                    value: 0.0,
                    backgroundColor: Colors.blue[100],
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            icon: const Icon(Icons.play_arrow, color: Colors.blue, size: 30.0),
            onPressed: () {
              log('Clicked');
            },
          ),
        ],
      ),
    );
  }

  Widget _buildDoneTasks() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 24.0, horizontal: 40.0),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(12.0),
        ),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'There are no completed tasks in this sprint',
            style: TextStyle(fontSize: 16.0),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 14.0),
          ConstrainedBox(
            constraints: const BoxConstraints.tightFor(width: 30, height: 30),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AddTask()),
                );
              },
              style: ButtonStyle(
                elevation: MaterialStateProperty.all(0),
                shape: MaterialStateProperty.all(const CircleBorder()),
                padding: MaterialStateProperty.all(EdgeInsets.zero),

                backgroundColor:
                    MaterialStateProperty.all(Colors.blue), // <-- Button color
                overlayColor:
                    MaterialStateProperty.resolveWith<Color?>((states) {
                  if (states.contains(MaterialState.pressed)) {
                    return Colors.blue[100];
                  }
                  return null;
                }),
              ),
              child: const Icon(Icons.add, size: 22.0),
            ),
          ),
        ],
      ),
    );
  }
}
