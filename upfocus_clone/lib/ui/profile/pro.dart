import 'package:flutter/material.dart';

class ProAccess extends StatefulWidget {
  const ProAccess({Key? key}) : super(key: key);

  @override
  State<ProAccess> createState() => _ProAccessState();
}

class _ProAccessState extends State<ProAccess> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: [
                    IconButton(
                        iconSize: 38.0,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(Icons.close)),
                    const Spacer(),
                    const Text(
                      'UP',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 38.0, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(width: 8.0),
                    Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 4.0),
                      decoration: const BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      ),
                      child: const Text(
                        'PRO',
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      ),
                    ),
                    const Spacer(),
                    const SizedBox(width: 38.0),
                  ],
                ),
              ),
              _buildBenefit(Icons.not_interested, 'No Ads!',
                  'Enjoy the app without ads!'),
              _buildDivider(),
              _buildBenefit(Icons.add_photo_alternate_outlined,
                  'Icons for goals', 'Add photos of your goals!'),
              _buildDivider(),
              _buildBenefit(Icons.cloud_upload_outlined, 'Auto-sync',
                  'Your data will be automatically synced when the internet is available!'),
              _buildDivider(),
              _buildBenefit(
                  Icons.insert_chart_outlined_rounded,
                  'Detailed statistics',
                  'More detailed statistics to keep track of your progress!'),
              _buildDivider(),
              _buildBenefit(Icons.mode_standby, 'More than 2 goals',
                  'With a subscription, you can set more than 2 goals!'),
              _buildDivider(),
              _buildBenefit(
                  Icons.account_tree_outlined,
                  'More than 4 sprints for each goal',
                  'With a subscription, you can set more than 4 sprints for each goal!'),
              _buildDivider(),
              _buildBenefit(
                  Icons.list_rounded,
                  'More than 8 tasks for each sprint',
                  'With a subcription, you can set more than 8 tasks for each sprint!'),
              _buildDivider(),
              _buildBenefit(Icons.star_border_rounded, 'Motivation',
                  'Payment is an additional motivation to achieve your goals!'),
              _buildDivider(),
              _buildBenefit(Icons.lightbulb_outline_rounded, 'Project support',
                  'For advertising, for further development'),
              const SizedBox(height: 20.0),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: Colors.blue,
                  elevation: 0,
                  side: const BorderSide(width: 1.0, color: Colors.blue),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32.0)),
                  minimumSize:
                      Size(MediaQuery.of(context).size.width / 1.5, 40),
                ),
                onPressed: () {},
                child: const Text('Continue'),
              ),
              const SizedBox(height: 12.0),
              const Text(
                'Try it for free for 7 days!',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
              ),
              const SizedBox(height: 8.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 60.0),
                child: Row(
                  children: const [
                    Expanded(
                      child: Text(
                        'After the end of the trial period, the payment will be made automatically and the subscription you selected above will be activated.\nThe trial period is given only once',
                        style: TextStyle(fontSize: 12.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 24.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDivider() {
    return const Divider(
      thickness: 0.7,
      indent: 118.0,
      endIndent: 20.0,
    );
  }

  Widget _buildBenefit(IconData icon, String title, String subtitle) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 60.0),
      child: Row(
        children: [
          Icon(icon, size: 42.0, color: Colors.blue),
          const SizedBox(width: 16.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title,
                    style: const TextStyle(
                        fontSize: 16.0, fontWeight: FontWeight.bold)),
                const SizedBox(height: 4.0),
                Text(
                  subtitle,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(fontSize: 13.0, color: Colors.black54),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
