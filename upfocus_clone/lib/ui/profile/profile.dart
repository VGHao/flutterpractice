import 'package:avatars/avatars.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:upfocus_clone/ui/profile/account.dart';
import 'package:upfocus_clone/ui/profile/pro.dart';
import 'package:upfocus_clone/ui/profile/settings.dart';
import 'package:upfocus_clone/ui/profile/statistic.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F6FB),
        body: Column(
          children: [_buildAppBar(context), _buildBody(context)],
        ),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      color: Colors.white,
      child: Row(
        children: const [
          Expanded(
            child: Text(
              'UP',
              style: TextStyle(fontSize: 38.0, fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            'Profile',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildUser(),
          const SizedBox(height: 16.0),
          _buildProfileList1(context),
          _buildProfileList2(context),
        ],
      ),
    );
  }

  Widget _buildUser() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 22.0),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30.0),
          bottomRight: Radius.circular(30.0),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Avatar(
            name: 'Vuong Gia Hao',
            shape: AvatarShape.rectangle(
                65, 65, const BorderRadius.all(Radius.circular(12.0))),
          ),
          const SizedBox(height: 16.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Vuong Gia Hao', style: TextStyle(fontSize: 18.0)),
              const SizedBox(width: 12.0),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                decoration: BoxDecoration(
                  color: Colors.blue[100],
                  borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                ),
                child: const Text(
                  'PRO',
                  style: TextStyle(color: Colors.blue, fontSize: 12.0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildProfileList1(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 16.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(12.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ProAccess()),
              );
            },
            child: _buildProfileRows('PRO access', Icons.star_border_rounded),
          ),
          const Divider(indent: 36.0),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Account()),
              );
            },
            child: _buildProfileRows('Account', Icons.account_box_rounded),
          ),
          const Divider(indent: 36.0),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Statistic()),
              );
            },
            child: _buildProfileRows('Statistics', Icons.insert_chart_rounded),
          ),
          const Divider(indent: 36.0),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Setting()),
              );
            },
            child: _buildProfileRows(
                'Settings', Icons.settings_applications_rounded),
          ),
        ],
      ),
    );
  }

  Widget _buildProfileList2(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 16.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(12.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              showModalBottomSheet<void>(
                context: context,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                backgroundColor: Colors.white,
                isDismissible: true,
                builder: (BuildContext context) {
                  return Container(
                    height: 150,
                    padding:
                        const EdgeInsets.only(top: 22, left: 20, right: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text(
                          'Contact us',
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 26.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            IconButton(
                              iconSize: 46.0,
                              onPressed: () {},
                              icon: const Icon(Icons.mail_outlined),
                            ),
                            IconButton(
                              iconSize: 46.0,
                              onPressed: () {},
                              icon: const Icon(FontAwesomeIcons.instagram),
                            ),
                            IconButton(
                              iconSize: 46.0,
                              onPressed: () {},
                              icon: const Icon(FontAwesomeIcons.vk),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              );
            },
            child: _buildProfileRows('Contact us', Icons.info),
          ),
          const Divider(indent: 36.0),
          InkWell(
            onTap: () {
              _launchUrl(
                  'https://play.google.com/store/apps/details?id=com.toprograms.up.focus');
            },
            child:
                _buildProfileRows('Rate the app', Icons.thumb_up_alt_rounded),
          ),
          const Divider(indent: 36.0),
          InkWell(
            onTap: () {
              _launchUrl(
                  'https://instagram.com/victorshamruk?igshid=YmMyMTA2M2Y=');
            },
            child: _buildProfileRows(
                'UP founder on instagram', FontAwesomeIcons.instagram),
          ),
          const Divider(indent: 36.0),
          InkWell(
            onTap: () {},
            child:
                _buildProfileRows('Our instagram', FontAwesomeIcons.instagram),
          ),
        ],
      ),
    );
  }

  Widget _buildProfileRows(String title, IconData icon) {
    return Row(
      children: [
        Icon(icon, color: Colors.blue, size: 28),
        const SizedBox(width: 12.0),
        Text(
          title,
          style: const TextStyle(fontSize: 16.0),
        ),
      ],
    );
  }

  void _launchUrl(String inputUrl) async {
    Uri url = Uri.parse(inputUrl);
    if (!await launchUrl(url)) {
      throw 'Could not launch $url';
    }
  }
}
