import 'package:flutter/material.dart';

class Setting extends StatefulWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  State<Setting> createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  bool isSyncSwitched = false;
  bool isAutostartSwitched = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color(0xFFF5F6FB),
        body: Column(
          children: [_buildAppBar(context), _buildBody(context)],
        ),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back_ios_new_rounded),
            iconSize: 32,
          ),
          const SizedBox(height: 8.0),
          const Padding(
            padding: EdgeInsets.all(18.0),
            child: Text(
              'Settings',
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.2,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 34.0, vertical: 28.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _firstSettingGroup(),
          const SizedBox(height: 16.0),
          _secondSettingGroup(),
        ],
      ),
    );
  }

  Widget _firstSettingGroup() => Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Column(
          children: [
            _buildTextSetting(
                'App theme', 'Light', Icons.format_paint_outlined),
            const Divider(indent: 20),
            Container(
              height: 35,
              padding: const EdgeInsets.all(4.0),
              child: Row(
                children: [
                  Container(
                    width: 26,
                    height: 26,
                    decoration: const BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    child: const Icon(
                      Icons.cloud_upload_outlined,
                      size: 18,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(width: 8.0),
                  const Text('Auto-sync'),
                  const Spacer(),
                  Switch(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: isSyncSwitched,
                    onChanged: (value) {
                      setState(() {
                        isSyncSwitched = value;
                      });
                    },
                    activeTrackColor: Colors.blue[100],
                    activeColor: Colors.blue,
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  Widget _secondSettingGroup() => Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Column(
          children: [
            _buildTextSetting('Podomoro Focus', '1 minutes'),
            const Divider(indent: 20),
            _buildTextSetting('Short Break', '5 minutes'),
            const Divider(indent: 20),
            _buildTextSetting('Long Break', '10 minutes'),
            const Divider(indent: 20),
            _buildTextSetting('Long After Break', '4 cycles'),
            const Divider(indent: 20),
            Container(
              height: 35,
              padding: const EdgeInsets.all(4.0),
              child: Row(
                children: [
                  const Text('Start automatically'),
                  const Spacer(),
                  Switch(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: isAutostartSwitched,
                    onChanged: (value) {
                      setState(() {
                        isAutostartSwitched = value;
                      });
                    },
                    activeTrackColor: Colors.blue[100],
                    activeColor: Colors.blue,
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  Widget _buildTextSetting(String title, String settingOption,
      [IconData? icon]) {
    return Container(
      height: 35,
      padding: const EdgeInsets.all(4.0),
      child: Row(
        children: [
          icon != null
              ? Container(
                  width: 26,
                  height: 26,
                  decoration: const BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                  ),
                  child: Icon(
                    icon,
                    size: 18,
                    color: Colors.white,
                  ),
                )
              : Container(),
          icon != null ? const SizedBox(width: 8.0) : Container(),
          Text(title),
          const Spacer(),
          Text(settingOption, style: const TextStyle(color: Colors.grey)),
        ],
      ),
    );
  }
}
