import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class TimerTab extends StatefulWidget {
  const TimerTab({Key? key}) : super(key: key);

  @override
  State<TimerTab> createState() => _TimerTabState();
}

class _TimerTabState extends State<TimerTab> {
  final CountDownController _controller = CountDownController();
  bool isStart = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildRecords(context),
          _buildTimer(context),
          _buildButton(),
          const SizedBox(height: 20.0),
        ],
      ),
    );
  }

  Widget _buildRecords(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24.0),
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Time in focus per day',
            textAlign: TextAlign.left,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
          const SizedBox(height: 12.0),
          ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: LinearProgressIndicator(
              value: 0.0,
              backgroundColor: Colors.blue[100],
            ),
          ),
          const SizedBox(height: 8.0),
          Row(
            children: const [
              Text(
                'Today: 0 s',
                style: TextStyle(fontSize: 14.0),
              ),
              Spacer(),
              Text(
                'Record: 0 s',
                style: TextStyle(fontSize: 14.0),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildTimer(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 100.0, bottom: 30.0),
          child: Center(
            child: CircularCountDownTimer(
              duration: 120,
              initialDuration: 0,
              controller: _controller,
              width: 280,
              height: 280,
              ringColor: Colors.grey[300]!,
              fillColor: Colors.blue,
              backgroundColor: Colors.transparent,
              strokeWidth: 12.0,
              strokeCap: StrokeCap.round,
              textStyle: const TextStyle(
                  fontSize: 58.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
              textFormat: CountdownTextFormat.MM_SS,
              isReverse: false,
              autoStart: false,
              onStart: () {
                debugPrint('Countdown Started');
              },
              onComplete: () {
                debugPrint('Countdown Ended');
              },
            ),
          ),
        ),
        Positioned.fill(
          top: 150,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 8,
                height: 8,
                decoration: const BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
              const SizedBox(width: 4.0),
              const Text(
                'Working',
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 12.0,
                    fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Visibility(
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          visible: isStart,
          child: RawMaterialButton(
            onPressed: () {
              setState(() {
                isStart = false;
                _controller.restart();
                _controller.pause();
              });
            },
            elevation: 0.0,
            fillColor: Colors.blue[100],
            padding: const EdgeInsets.all(8.0),
            shape: const CircleBorder(),
            child: const Icon(
              Icons.stop_rounded,
              size: 32.0,
              color: Colors.blue,
            ),
          ),
        ),
        RawMaterialButton(
          onPressed: timerToggle,
          elevation: 0.0,
          fillColor: isStart ? Colors.blue[100] : Colors.blue,
          padding: const EdgeInsets.all(12.0),
          shape: const CircleBorder(),
          child: Icon(
            isStart ? Icons.pause : Icons.play_arrow,
            size: 36.0,
            color: isStart ? Colors.blue : Colors.white,
          ),
        ),
        Visibility(
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          visible: false,
          child: RawMaterialButton(
            onPressed: () {},
            elevation: 0.0,
            fillColor: Colors.blue[100],
            padding: const EdgeInsets.all(8.0),
            shape: const CircleBorder(),
            child: const Icon(
              Icons.skip_next_rounded,
              size: 32.0,
              color: Colors.blue,
            ),
          ),
        ),
      ],
    );
  }

  void timerToggle() {
    setState(() {
      isStart = !isStart;
      if (isStart) {
        _controller.start();
        Fluttertoast.showToast(
          msg: "Get to work!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 14.0,
        );
      } else {
        _controller.pause();
      }
    });
  }
}
