mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value!)) {
      return 'Pls input valid email.';
    }
    return null;
  }

  String? validatePassword(String? value) {
    int minLength = 8;
    bool hasUppercase = value!.contains(new RegExp(r'[A-Z]'));
    bool hasLowercase = value.contains(new RegExp(r'[a-z]'));
    bool hasSpecialCharacters = value.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool validLength = value.length >= minLength;

    if (hasUppercase & hasLowercase & hasSpecialCharacters & validLength) {
      return null;
    }
    return 'Password must contain at least: \n'
        '- 1 uppercase(A-Z)\n- 1 lowercase(a-z)\n'
        '- 1 special character(!@#\$%^&*(),.?":{}|<>)\n'
        '- More than 8 characters';
  }
}